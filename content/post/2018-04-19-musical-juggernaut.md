---
title: The Chronicles of a Musical Juggernaut
date: 2018-04-19
---

so i was told a few days (maybe a week?) ago by [a good friend of mine](https://teraflonk.github.io/), in response to the my announcement of the work-in-progress "Big Fish To Grill" concept album, that I am a sort of "musical juggernaut".

"Whoa, another concept album on the way. You're like a musical juggernaut dude. I don't know how you manage to do it all!"

ok, this conversation definitely took place a week ago, so why it's coming to mind now is a bit of a mystery lmao. but i wanted to spend some time today on 420-eve expanding on this conversation; mainly just streamlining some insights into my creative process.

when tf did i open a duckduckgo tab; im not even searching anything.

anyways, people who have known me for atleast a little while know that i write a lot of music. like, *a lot*. please redirect yourself to this website's discography page (under the music tab) to see a sliver of what i'm talking about. i have a bit more frequent output than most of my internet friends as a result.

take note however that i release a lot more "underpolished" material per say (ie. last month when i recorded a banjo cover album a week after i got a banjo).

i think it's easy as a musician to constrain yourself into boxes. hell, i'm always thinking ahead to the next main-line "Mark! Silver" album in the back of my mind, which is kinda counterintuitive for starting new ideas, but is a helpful goal to have so i can actually finish stuff. but sometimes y'just gotta let your mind run wild. i suppose this is nothing groundbreaking, but i wanted to iterate that here anyways.

one day i might be thinking of contemplative lyrics for an acoustic album, & the next day i might wake up wanting to write a bunch of ska, only to open up LMMS & work on some hip-hop instrumentals a few hours later. but the key to it all is letting my insane ideas take the wheel. sure, i definitely will not use over 90% of the material generated when putting together a polished release, but it gives me a pretty large pool of ideas to choose from when it comes time to make the next big album, so to speak.

if i placed fake ads on this website, would you be enticed to buy their non-existant products?

ignore the above sentence; im just thinking out loud. though maybe said above sentence would make a good lyric? haha, i guess that touches on another (but closely-related) point: write everything down.

well, not everything. if you have some weird fantasy about riding off into the sunset on a horse w/ cans of ridiculously overpriced shaving cream, maybe [your neighbor](https://www.nsa.gov/) will raise an eyebrow or three.

but again, the more stuff you have written down, the more you have to pull from.

ALSO, do you ever have those moments where you have a really good song idea in your head & dont wanna forget it. coz i do, & i will literally stop whatever else i was doing (unless it's like eating, showering, etc) to go grab [my laptop](https://system76.com/) or my notebook (but almost always my laptop) & jot down whatever came to mind. 'probably another "duh" thing, but it's super important.

i sometimes like to go back to lyrics that came from some 12-in-the-am-pm stream of consciousness text file & wonder what the hell i was thinking or how the hell the melody goes or the rhythm feels. i can have a good laugh at myself sometimes, & merely hope that these particular lyrics are useful in the future (they sometimes are, but i wont bet on that).

[another good friend of mine](https://christophertom.bandcamp.com/) told me that Weezer's lead singer & songwriter Rivers Cuomo just [writes a bunch of ideas](https://www.weezerpedia.com/wiki/Pacific_Daydream) on a spreadsheet w/ no particular album in mind so he can come back to it later. i havent tried this method, but it sounds like a good idea, so i thought i'd share that here.

anyhoo, 420 is tomorrow, & i plan to write a post about it (i will not be participating in the weed-smoking unfortunately), so stay tuned. :)
