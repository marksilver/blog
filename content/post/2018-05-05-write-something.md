---
title: Write Something - Boosting Your Creative Output As A Songwriter (Part 1)
date: 2018-05-05
---

I want to take the opportunity to share a songwriting technique that I've adopted recently in my creative process as a songwriter, as well as the related rationale that governs my writing.

I had heard some-odd months ago about how Weezer's frontman Rivers Cuomo goes about writing songs, & it peaked my interest. Basically, he keeps a spreadsheet of lines that don't belong to any particular song or album, & tags them for later use.

What interested me about his process is how radically it differs from my approach. More often than not, I'll start writing down a song idea for a particular project & said idea will remain in the scope of its particular project (until I start merging projects into a pool to pick from for an album). I decided that I would give this spreadsheet method a go about a week ago to see how this would differ from my traditional approach.

On my spreadsheet, each lyric idea I have is partnered with what key/chord I want it to go with, as well as a song that I've either heard or made before that I have in mind for the style to keep in mind. Beyond that, no idea belongs to a particular project, as the goal is to write something.

And that's the crux of it for me, I'm quite certain: writing for the sake of writing has its perks to writing for a specific album. The spreadsheet method has given me the drive to write literally whatever comes to mind. If an idea is a nonstarter, then that it stays, and if an idea is really good, I will write additional lines to supplement it. Because the flow of ideas is more important than my intrinsic pressure to write a song for a specific project, I'm able to generate more output to begin with -- much of which probably wouldn't exist if I wasn't writing on this spreadsheet.

Presently, my spreadsheet consists of 72 lines, among which there are 17 groupings of two or more lines that I could turn into a great song, which I'm convinced will be very helpful in my process of grouping song ideas into projects that eventually get streamlined into albums. Rather than writing a song with a project in mind, I can form a new project around ideas I already have, thus shifting my focus from the end result to the journey of getting there.

But in the scope of improving creative output, starting things is half the battle. Turning ideas into recordings/releases is a whole nother aspect, and I intend to write one or two follow-up posts discussing this in great detail. I hope that my anecdote about my new approach to the writing process inspires you to get out there & do something to that effect. Seriously though; write something! ;)
