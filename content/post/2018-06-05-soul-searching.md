---
title: Searching For A Soul (EP)
date: 2018-06-05
---

So I recently came across this nifty little website called Fanburst. It seems to me to function like Soundcloud, but without the upload limit, and perhaps a bit more of a streamlined interface. I figured that to test out how this site works, I'd release a little something I've been meaning to get out into the wild for a little while now:

<iframe src="https://fanburst.com/marksilver/album/searching-for-a-soul-ep/embed" width="650" height="250" frameborder="0"></iframe>

"Searching For A Soul" is the name for this collection of acoustic demos, which I had brief intentions to expand into a spiritual succesor to my "Mark Silver's Summer Acoustic Demos" album. However, as I recorded more prospective tracks, it became more apparent that the four tracks that made up this selection from the beginning are really best kept as these four tracks; short & sweet, you might say.

The first two tracks of this release will be receiving more fleshed out arrangements for my "Blue Masquerade" album, and if all things go as planned, I should be able to get that album released around August 2019 or maybe even a bit earlier.

The last two tracks of this EP are not slated for Blue Masquerade, but they are still some great introspective tunes that are worth your time, and I fully intend to include them in live sets in the future.

As always, there's always more music right around the corner, so stay tuned. ;)
