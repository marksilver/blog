---
title: Big Fish To Grill - Concept Demo
date: 2018-04-09
---

even though the Blue Masquerade album is my primary project atm, im still writing new stuff all the time. one of my more recent side projects is more or less of a concept album surrounding the ocean, using more dynamic arrangement & poetic license than my flagship project.

<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/427439544&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>

"Big Fish To Grill" is the title track for said concept album, & is quite the oddity among my collection; using four time signatures & an excessive amount of aleatoric fermatas.

stay tuned; there's more on the horizon.
