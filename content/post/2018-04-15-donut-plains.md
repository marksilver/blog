---
title: Donut Plains (Funky Jam Cover)
date: 2018-04-15
---

if you've played the original mario kart game, you may very well remember the theme to its "Donut Plains" tracks... but have you heard it quite like this before?

<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/430255563&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>

i listen to a lot of vulfpeck. i came across them by accident browsing youtube one day, & haven't stopped listening since. seriously, vulfpeck brings the FUNK. i've also been following some of their tutorials on how to play some of their songs, & i more or less began adapting a bit of that style into my playing.

anyways, in my spare time, i like to sit down at my piano & jam out on whatever comes to mind. about a week or two ago, i got the idea to play a real funky version of this mario kart classic. over the past few days, i've been tweaking the structure & arrangement of the song to see just *how* funky i could make it, & today i thought it was in a state where i could record a demo out of it.

i am literally listening to this on loop right now, and you should too.

as always, there's more to come in the realm of music, so stay tuned. ;)
