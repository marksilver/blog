---
title: Goal Setting - Boosting Your Creative Output As A Songwriter (Part 3)
date: 2018-05-07
---

So you have a bunch of song ideas & demos recorded. Now what?

In my workflow as a musician, it is very easy to accumulate a large-pool of songs in the form of written lyrics & recorded demos. And for some, that's all there is too it. However, I maintain the belief that every artist should record a full-length album at some point in their life, and in this final part of this "Boosting Your Creative Output As A Songwriter" series, I'll explain why.

Think now of what the circumstance may entail. You've been making tons of songs, & you may want to put out something to show for it. If you're wholly dedicated to making an album as a long-term project, the process involves sorting through to find the cream of the crop, & becoming intimately familiar with the songs you've selected. Or atleast that's how it works for me. I record alot of acoustic demos & rough sketches, but in order for me to include it in an LP lineup, I personally need to have a vision for what direction in which I'd like to take the song. In addition to that, I feel inclined to put my best foot forward with the production side of things. In the case of my [self-titled LP](https://marksilver.bandcamp.com/album/mark-silver), this involved sequencing the background instrumentals, doing multiple takes of guitars & vocals, and refining the tunes extensively in post-production.

But why am I relaying all this information? Why am I going on about my album-making process? I want to reiterate that making an album may not necessarily be in your plans. However, having a long-term goal in mind has been very helpful for me in the process of turning ideas & sketches into really great songs. And whilst I'm always coming up with new side-projects to mess around with on the side, there's always a motivation within me to work on my main project, which is always a particular album.

And heck, it feels so great too! When I finished work on the aforementioned self-titled LP, it was really rewarding to be able to release something that showcases the best of my musical efforts over the preceding two years. That kind of delayed gratification is the stuff that really empowers me, and is one of the things in my life I can truly take pride in.

Well, that concludes this three-part series on boosting your creative output as a songwriter. Albeit consisting mostly of my personal anecdotes & whatnot as a musician, I hope something I said in one of these posts inspires you to get out there and create something great!

As always, there's more to come (whether music or blog posts or whatever), so stay tuned. ;)
