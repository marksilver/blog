---
title: Keeping Motivated - Boosting Your Creative Output As A Songwriter (Part 2)
date: 2018-05-06
---

Creating music is very much a passion for me. But with all sorts of other things keeping me busy in my day-to-day life, it's hard sometimes to muster up the energy to actually create something. Today, I want to very briefly discuss a few things I do to keep myself creating, even when I'm absolutely drained physically and mentally.

Whenever I'm in a rut, I find it helpful to engage in tasks that keep me accountable to myself in some way. This probably sounds ambiguous, but allow me to elaborate with examples: Every year, I participate in [February Album Writing Month](http://fawm.org/) (FAWM), which pushes users to write fourteen songs within twenty-eight days. It sure sounds like a daughnting task, and I have completed it successfully in each of the three years I've participated in thus far. Having that fixed goal in place means that I can't say to myself "oh, I'll do this later", because there won't be any time later. Of course, this comes back to what I said yesterday about [creating for the sake of creating](https://marksilver.gitlab.io/post/2018-05-05-write-something/).

One thing I would like to do more often than I lately have been is to livestream myself creating music. Whether it be writing MIDIs or sheet music, I have this nasty habit of letting myself get distracted & putting off finishing alot of what I start. However, when you livestream, anyone can pop in and see what you're up to, which always compells me to stay focused for longer durations of time on what I want to achieve.

One more thing that I've tried (albeit with marginally less output than the previous two strategies) is taking a break from the Internet for a few days to a week at a time. If you're like me, you spend a lot of time at your computer, but browse assorted websites too much for your own good. Being disconnected from the net, I can spend some of this free time doing more productive things than surfing said net. The only thing though that makes this strategy less optimal is that you could still get distracted by other things whilst offline, but I thought I'd put this strategy here nonetheless. If nothing else, it'll help to clear your mind a bit.

I hope that these strategies I've made use of can help you in boosting your creative output. Whether you're a musician or artist or writer or whatever other creative types are out there, be looking for opportunities to try strategies like these to help your focus & stay motivated. Tomorrow, I'll conclude this micro-series of posts with a bit about goal setting & finishing the projects you start, so keep on reading! ;)
