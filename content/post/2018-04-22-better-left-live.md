---
title: Some Things Are Better Left Live (+ Blue Masquerade Album Progress Update)
date: 2018-04-22
---

idk if people actually read this blog, but it'd be real sweet to build a public portfolio of informal writing about music to supplement the music i put out there.

so i had a bit of a wild gig on saturday, becoz i would be involved w/ three acts each playing a song at a different point within the first half of the show. i wont ramble on the details, but i did a solo piece as the second of three acts i was involved in, playing an updated arrangement of the "Blue Masquerade / Ambition" song (well, technically two songs, but one fades to the other). i do not presently have a recording of last night's performance, but an older version of the tune can be found in the "Blue Masquerade (Demos)" album in my discography, as well as on my soundcloud page.

on friday though, myself & the other acts participated in a dress rehearsal for the show, and my thing took like 7 minutes, so i had quite a bit of downtime during the rest of the run. and that, my friends, is where the magic happened.

upon arrival, i was prompted to play Oasis' "Wonderwall" on guitar + vox whilst being backed up by a drummer & another vocalist. almost out of the blue, myself & assorted musicians waiting with me in this room proceded to play a bunch of old & new pop tunes on assorted instruments (+ vox). from The Beatles' "Hey Jude" to Ed Sheeran's "A-Team" to Ceelo Green's "Forget You" to Ween's "Ocean Man" to the obligatory Billy Joel classic "Piano Man". like, we were all just goofin' around & havin' a good time 'n' stuff, & it was pretty great i think.

i wanted to relay this perhaps arbitrary information to a blog post today becoz i think it gets to the heart of why im a musician; becoz it's all sorts of fun all the time.

like seriously, this "jam session", if you could call it that, was more high-energy than the gig the next day. perhaps becoz everyone in the audience during this jam was as much a participant as those who were playing instruments (becoz they were singing along, silly). it was real unique i think atleast for me becoz im not quite accustomed to such tight audience-performer interaction, which is a welcome change even if there were only like 7 people in the room.

i was thinking about seeing if someone would be willing to record this jam so i could put it on this website, but im not 100% sure that'd have been apt, which brings an important distinction to my consciousness. you could listen to a live performance of any song through a recording of it & hear pretty much everything that everyone heard, but be completely missing out on how it felt to be there in the moment it happened. heck, if we recorded this session, i could go back to it & listen once again, but i think it loses its special touch that way.

idk; jus' thinkin' out loud.

anyways, i wanted to give a little progress update on the Blue Masquerade album. as i indicated in the demo collection for this project (again, see my discography), the next step is writing sheet music for these songs so i can record 'em w/ fellow musicians. i dont have a whole lot done in that regard, & i'll probably sort of start from scratch w/ the musescore files i do have for this album. this is becoz i want to take a bit of a more constrained approach to arranging this album. in other words, writing down for each track what instruments i can write for:

![constrained writing yoooo](/img/BMband.png)

the idea here is to give myself a bit more particular focus w/ the arranging process, as well as to force myself to concentrate on bringing out the best of what i have to work with. when it comes time to record & subsequently mix this album, if i think there's something missing in the mix, i can add it in all impromptu. after all, on my s/t, the "woooo"s on "Straw In The Ocean" & pretty much the entirety of the instrumentation on "Through The Swamp" and "Who Needs Magic Anyways?" were recorded spontaneously.

alrighty, well that's all for today folks! tune in next time for some more probably weird musical adventures, almost entirely typed in lowercase becoz im a dweeb lmao.
