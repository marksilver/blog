---
title: Returning To Old Ideas With A Fresh Mind
date: 2018-05-12
---

As much as I've written this past week about moving forward in the process of developing new ideas for songs, I want to talk a little bit about returning to older songs you've with a brand new state of mind.

I've been thinking over the past few days about how I would play some of the songs from [my eponymous album](https://marksilver.bandcamp.com/album/mark-silver) as a live set w/ a piano + vocal setup. So I sat down at the ol' piano to hash out these older tunes, as one might do in this situation.

I should add some context that will help me progress this story. Lately, I've been listening to alot of songs containing elements of funk, soul, gospel, & et cetera of the like (including some more modern spins infused with hip-hop). Create a Pandora station with Vulfpeck, Stevie Wonder, and Childish Gambino to hear kind of what I'm talking about. Over the past few months, these styles have taken my piano playing in a whole new direction, so my casual improv and impromptu covers have been influenced in this light.

As I sat down to play some of the songs from the my eponymous LP, I was having all sorts of fun incorporating some of these influences into what are songs that I had worked so vigorously with this past summer. From adding some melodic-esque work to the left hand to reharmonizing many of the chord progressions, these old songs of mine, some of which having been conceived almost three years ago now, began to feel fresh once again.

There is something to be said about songs, like the instruments they're created on, being used themselves as an instrument of creation. It's an argument I've seen more often when talking about the plunderphonics and vaporwave genres, but I think it can be said about the anecdote I've shared today. When you create a song, whether a short lyric or a finished product, you are building to your portfolio of ideas which you can pull from later; because something new might come to you that brings these ideas into a whole new light.

I hear stories of successful musicians who got sick & tired of playing the same songs over & over at shows. Whilst I'm sure that I'd be able to relate if I actually played more gigs, I think that this look to playing older songs fails to take advantage of said songs' creative potential. Perhaps you could take the opportunity to change up the arrangement or style of the song, or maybe use the end of it to fade into a newer tune, thus riding the hype of the previous one? Something to think about, I suppose.

I didn't have a single particular point with posting this today, but I hope it can provide some interesting insight that may be useful. As always, there's more to come, so stay tuned. ;)
